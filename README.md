# reflowPCB

Reflow a PCB with a PCB

Idea taken from: https://www.youtube.com/watch?v=dy9NutXl2OY

# Principal

A large coil-shaped track is used to heat up a PCB that should be populated with SMT components.
This required two components:
- a PCB with the coil
- a driver PCB

I want to keep the two PCBs separate to be able to debug one without the other.

# Coil

Many options for the coil shape exist!
However, I think for the beginning a rectangular spiral that goes inwards and then outwards might be the best option.

```
┃┃┏━━━━┓
┃┃┃┏━━┓┃
┃┃┃┃┏┓┃┃
┃┃┃┃┃┃┃┃
┃┃┃┗┛┃┃┃
┃┃┗━━┛┃┃
┃┗━━━━┛┃
┗━━━━━━┛
```

# Driver

Should consist only of a low-side MOSFET, a microcontroller, and a thermocouple in the beginning.
Then close the loop from µC -> MOSFET -> coil -> thermocouple -> µC using a simple control law - maybe PD is enough?

Later I might want to add a fan control too to be able to actively cool the thing down.
